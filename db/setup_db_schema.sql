create table comment (
  comment_id serial primary key,
  creator_name text not null,
  message text not null,
  creation_timestamp timestamp default current_timestamp
);