#!/usr/bin/env bash

# network to connect server and database
docker network create --driver bridge postgres-network


# set up database
cd db
docker build -t haus-challenge-db .
cd ..

# set up server
cd server
docker build -t haus-challenge-server .
cd ..

# start both
docker run --network postgres-network --name haus-challenge-db1 -d haus-challenge-db
docker run -p 9000:3001 --network postgres-network --name haus-challenge-server1 -d haus-challenge-server

# start client
cd client
yarn install
yarn start