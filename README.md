# Haus Code Challenge

This is composed of a front-end implemented in React, a server using node.js and express, and a postgres database.

User authentication is provided by Auth0.

On the main page, any user (including anonymous) can see all of the comments.

Once a user is logged in, they can go to the "My Comments" tab to see all of their comments.

Logged-in users can post a comment from either page.

To run locally, make sure you have Docker and yarn installed, then run `./run_me.sh` from the main directory.
This script builds and runs Docker containers for the server and database, but runs the client in development mode
due to time constraints.

I was planning to deploy this on Heroku, but I didn't realize that you can only run two containers at a time on the
free plan, so I'd have to re-architecture this to either use some other cloud data store, or the Heroku postgres plugin.

Also, I've never used node.js/express to develop a server, so a significant amount of my time was sunk into reading docs for that.