'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const jwt = require('express-jwt');
const jwksRsa = require('jwks-rsa');
var cors = require('cors');
const commentService = require('./services');

// Express instance to manage the server
const app = new express();

// Accept JSON data
app.use(bodyParser.json());

// For development
app.use(cors());

const checkJwt = jwt({
  secret: jwksRsa.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: `https://andrew-gleeson.auth0.com/.well-known/jwks.json`
  }),

  // Validate the audience and the issuer.
  audience: 'kxQodmls3dWuAHUz-TyAqKHcYkvb-ZKe',
  issuer: `https://andrew-gleeson.auth0.com/`,
  algorithms: ['RS256']
});


app.post('/comment', checkJwt, (req, res) => {
  if (!req.body || !req.body.message) return res.sendStatus(400);
  commentService.addComment(req.user.name, req.body.message, res);
});

app.get('/mycomments', checkJwt, (req, res) => {
  commentService.getUserComments(req.user.name, res);
});

app.get('/', (req, res) => {
  commentService.getAllComments(res);
});

app.listen(3001, () => {
  console.log('Server up!');
});