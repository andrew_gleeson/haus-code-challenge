'use strict';

const {Pool} = require('pg');
const axios = require('axios');

// A connection pool to query the database
const pool = new Pool({
  user: 'postgres',
  host: 'haus-challenge-db1',
  database: 'postgres',
  password: 'haus',
  port: 5432
});

const addComment = (creatorName, message, res) => {
  pool.query(`insert into comment (creator_name, message) values ($txt$${creatorName}$txt$, $txt$${message}$txt$);`, (err, queryResult) => {
    console.log(`New comment by ${creatorName}: ${message}`);
    res.send(queryResult);
  });

  axios.post('https://hooks.slack.com/services/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', {
      text: `${creatorName}: "${message}"`
    })
    .then((res) => {
      console.log('Comment forwarded to Slack successfully');
    })
    .catch((error) => {
      console.error(error)
    })
};

const getAllComments = (res) => {
  pool.query('select * from comment order by creation_timestamp desc;', (err, queryResult) => {
    res.send(queryResult.rows);
  });
};

const getUserComments = (creatorName, res) => {
  pool.query(`select * from comment where creator_name=$txt$${creatorName}$txt$ order by creation_timestamp desc;`, (err, queryResult) => {
    res.send(queryResult.rows);
  });
};

module.exports = {addComment, getAllComments, getUserComments};