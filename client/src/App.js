import React, {Component} from 'react';
import {BrowserRouter, Route} from 'react-router-dom';
import './App.css';
import {Layout} from 'antd';

import AllComments from './components/AllComments';
import Callback from "./components/Callback";
import Navbar from "./components/Navbar";
import MyComments from "./components/MyComments";

import 'antd/dist/antd.css';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <Layout>
            <Layout.Header>
              <Navbar/>
            </Layout.Header>
            <Layout.Content style={{paddingTop: '15px'}}>
              <Route exact path="/" component={AllComments}/>
              <Route exact path="/mycomments" component={MyComments}/>
              <Route exact path="/callback" component={Callback}/>
            </Layout.Content>
            <Layout.Footer>made by Andrew Gleeson</Layout.Footer>
          </Layout>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
