import React, {Component} from 'react';
import auth0Client from './Auth';
import {Row, Col, Button, Icon} from 'antd';
import styled from "styled-components";
import {Link, withRouter} from "react-router-dom";

const Title = styled.div`
  font-size: 25px;
  color: palevioletred;
  float: left;
`;

const UserPanel = styled.div`
  color: white;
  float: right;
`;

const NavLink = styled(Link)`
  color: white;
  text-decoration: none !important;
  margin-left: 10px;
  margin-right: 10px;
`;


class Navbar extends Component {
  signOut = () => {
    auth0Client.signOut();
    this.props.history.replace('/');
  };

  render() {
    console.log(auth0Client.getProfile());
    return (
      <Row>
        <Col span={8}><Title>Haus Code Challenge</Title></Col>
        <Col span={8}>
          <NavLink to="/"><Icon type="bars" /> Home</NavLink>
          {auth0Client.isAuthenticated()
            ? <NavLink to="/mycomments"><Icon type="inbox" /> My Comments</NavLink>
            : null
          }
        </Col>
        <Col span={8}>
          <UserPanel>
            {auth0Client.isAuthenticated()
              ? (
                <div>
                  <Icon type="user"/>
                  <span style={{marginLeft: '10px', marginRight: '10px'}}>{auth0Client.getProfile().nickname}</span>
                  <Button icon="logout" ghost={true} onClick={() => this.signOut()}/>
                </div>
              )
              : (
                <Button icon="user" ghost={true} onClick={auth0Client.signIn}>Sign In</Button>
              )
            }
          </UserPanel>
        </Col>
      </Row>
    );
  }
}

export default withRouter(Navbar);