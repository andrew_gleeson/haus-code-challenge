import React, {Component} from 'react';
import auth0Client from './Auth';
import {Input} from "antd";

class AddCommentForm extends Component {

  state = {
    message: null
  };

  addComment = () => {
    fetch('http://localhost:9000/comment',
      {
        method: 'post',
        headers: {
          'Authorization': `Bearer ${auth0Client.getIdToken()}`,
          'Access-Control-Allow-Origin': '*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({message: this.state.message})
      }
    ).then(() => this.setState({message: null}));
  };

  onChange = (message) => {
    this.setState({message});
  };


  render() {
    return (
      <div>
        <Input placeholder="Add your comment (enter to submit)" value={this.state.message}
               onChange={e => this.onChange(e.target.value)} onPressEnter={() => this.addComment()}/>
      </div>
    );
  }
}

export default AddCommentForm;