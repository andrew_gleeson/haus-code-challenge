import React from 'react';

import {Card} from "antd";

export default ({creatorName, message}) => (
  <Card
    size="small"
    title={creatorName}
    style={{marginTop: '5px', marginBottom: '5px'}}
  >
    {message}
  </Card>
);