import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import auth0Client from './Auth';
import {Spin, Row, Col, Divider} from "antd";
import AddCommentForm from "./AddCommentForm";
import Comment from "./Comment";

class AllComments extends Component {

  state = {
    comments: null
  };

  getAllComments = () => {
    fetch('http://localhost:9000/')
      .then(response => response.json())
      .then(comments => this.setState({comments}));
  };

  componentDidMount() {
    this.timer = setInterval(()=> this.getAllComments(), 1000);
  };

  componentWillUnmount() {
    clearInterval(this.timer);
    this.timer = null;
  }

  render() {
    if (this.state.comments === null) {
      return <Spin/>;
    }

    return (
      <div>
        <Row>
          <Col span={6}/>
          <Col span={12}>
            <Divider>All Comments</Divider>
            {auth0Client.isAuthenticated() ? <AddCommentForm/> : null}
            {this.state.comments === null ? <Spin/> : this.state.comments.map(comment => (
              <Comment key={comment.comment_id} creatorName={comment.creator_name} message={comment.message}/>
            ))}
          </Col>
          <Col span={6}/>
        </Row>
      </div>
    );
  }
}

export default withRouter(AllComments);