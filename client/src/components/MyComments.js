import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import auth0Client from './Auth';
import {Spin, Row, Col, Divider} from "antd";
import AddCommentForm from "./AddCommentForm";
import Comment from "./Comment";

class MyComments extends Component {
  state = {
    comments: null
  };

  getMyComments = () => {
    fetch('http://localhost:9000/mycomments', {
      headers: {
        'Authorization': `Bearer ${auth0Client.getIdToken()}`,
      }
    })
      .then(response => response.json())
      .then(comments => this.setState({comments}));
  };

  componentDidMount() {
    this.timer = setInterval(()=> this.getMyComments(), 1000);
  };

  componentWillUnmount() {
    clearInterval(this.timer);
    this.timer = null;
  }

  render() {
    if (!auth0Client.isAuthenticated()) {
      return null;
    }

    if (this.state.comments === null) {
      return <Spin />;
    }

    return (
      <div>
        <Row>
          <Col span={6}/>
          <Col span={12}>
            <Divider>My Comments</Divider>
            <AddCommentForm/>
            {this.state.comments === null ? <Spin/> : this.state.comments.map(comment => (
              <Comment key={comment.comment_id} creatorName={comment.creator_name} message={comment.message}/>
            ))}
          </Col>
          <Col span={6}/>
        </Row>
      </div>
    );
  }
}

export default withRouter(MyComments);